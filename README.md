# Inf Workshop LaTeX

This repository uses CI/CD to auto generate a PDF from a .tex file.

## Getting started
On any alterations on the repository, the CI scripts will check the repository for any .tex files and generate homonimous pdf files from them.
For instance, the example file "inf_article.tex" will be converted to "inf_article.pdf".

Such files are only accessbile through the artifacts link (i.e. they are not added to the repository). To access the files use the link https://gitlab.gwdg.de/pedro.costaklein/inf-tutorial-latex/-/jobs/artifacts/main/raw/YOUR_FILENAME.pdf?job=compile_pdf, replacing YOUR_FILENAME by the name of the tex file you want to donwload.


Link to the processed [inf_article.tex](https://gitlab.gwdg.de/pedro.costaklein/inf-tutorial-latex/-/jobs/artifacts/main/raw/inf_article.pdf?job=compile_pdf) file
